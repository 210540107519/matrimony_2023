//import 'dart:html';

import 'package:final_app/database/database.dart';
import 'package:final_app/model/Eduaction_model.dart';
import 'package:final_app/model/State_model.dart';
import 'package:final_app/model/city_model.dart';
import 'package:final_app/model/country_model.dart';
import 'package:final_app/model/profession_model.dart';
import 'package:final_app/model/realigion_model.dart';
import 'package:final_app/model/user_model.dart';
import 'package:final_app/user_list_page.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/src/dropdown_button2.dart';

class Adduser extends StatefulWidget {
  late UserModel? model;

  Adduser({required this.model});

  @override
  State<Adduser> createState() => _AdduserState();
}

class _AdduserState extends State<Adduser> {
  CityModel model = CityModel(
    CityID1: -1,
    CityName1: '',
  );

  late statemodel model1 = statemodel(State_ID1: -1, State_NAME1: '');
  late Countrymodel model2 = Countrymodel(Country_ID1: -1, Country_NAME1: '');
  late Realigonmodel model3 =
      Realigonmodel(Religion_ID1: -1, Religion_Name1: '');
  late Eduactionmodel model4 =
      Eduactionmodel(Education_ID1: -1, Education_NAME1: '');
  late Professionmodel model5 =
      Professionmodel(Profession_ID1: -1, Profession_NAME1: '');

  bool isGetCity = true;
  bool isGetState = true;
  bool isGetCountry = true;
  bool isGetReligion = true;
  bool isGetEduaction = true;
  bool isProfession = true;
  //bool
  final _formKey = GlobalKey<FormState>();
  late TextEditingController nameController;
  late TextEditingController heightController;
  late TextEditingController weightController;
  late TextEditingController stateController;
  late TextEditingController countryController;
  late TextEditingController religionController;
  late TextEditingController educationController;
  late TextEditingController professionController;
  late TextEditingController genderController;
  late TextEditingController dobController;

  //late TextEditingController nameController;
  MyDatabase myDatabase = MyDatabase();

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(
        text: widget.model != null ? widget.model!.User_NAME1.toString() : '');

    heightController = TextEditingController(
        text:
            widget.model != null ? widget.model!.User_HEIGHT1.toString() : '');

    weightController = TextEditingController(
        text:
            widget.model != null ? widget.model!.User_WEIGHT1.toString() : '');

    stateController = TextEditingController(
        text: widget.model != null ? widget.model!.State_ID1.toString() : '');

    countryController = TextEditingController(
        text: widget.model != null ? widget.model!.Country_ID1.toString() : '');

    religionController = TextEditingController(
        text:
            widget.model != null ? widget.model!.Religion_ID1.toString() : '');

    educationController = TextEditingController(
        text:
            widget.model != null ? widget.model!.Education_ID1.toString() : '');

    professionController = TextEditingController(
        text: widget.model != null
            ? widget.model!.Profession_ID1.toString()
            : '');

    genderController = TextEditingController(
        text:
            widget.model != null ? widget.model!.User_GENDER1.toString() : '');

    dobController = TextEditingController(
        text: widget.model != null ? widget.model!.User_DOB1.toString() : '');
  }

  @override
  Widget build(BuildContext context) {
    print('DEMO::1::${model.City_NAME.toString()}');
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Add user'),
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: double.infinity,
                  child: FutureBuilder<List<CityModel>>(
                    builder: (context, snapshot) {
                      print('DEMO::2::${model.City_NAME.toString()}');
                      if (snapshot.data != null && snapshot.hasData) {
                        print('DEMO::3::${model.City_NAME.toString()}');
                        if (isGetCity) {
                          model = snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            items: snapshot.data!
                                .map((item) => DropdownMenuItem<CityModel>(
                                      value: item,
                                      child: Text(
                                        item.City_NAME.toString(),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ))
                                .toList(),
                            value: model,
                            onChanged: (value) {
                              setState(() {
                                isGetCity = false;

                                model = value!;
                              });
                            },
                            buttonStyleData: ButtonStyleData(
                              height: 50,
                              width: 160,
                              padding:
                                  const EdgeInsets.only(left: 14, right: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                border: Border.all(
                                  color: Colors.black26,
                                ),
                                color: Colors.cyanAccent,
                              ),
                              elevation: 2,
                            ),
                            iconStyleData: const IconStyleData(
                              icon: Icon(Icons.arrow_drop_down),
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    future: isGetCity ? myDatabase.getCityListFromTbl() : null,
                  ),
                ),

                // secound dropdown
                Container(
                  width: double.infinity,
                  child: FutureBuilder<List<statemodel>>(
                    builder: (context, snapshot) {
                      print('DEMO::2::${model1.State_NAME.toString()}');
                      if (snapshot.data != null && snapshot.hasData) {
                        print('DEMO::3::${model1.State_NAME.toString()}');
                        if (isGetState) {
                          model1 = snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            items: snapshot.data!
                                .map((item) => DropdownMenuItem<statemodel>(
                                      value: item,
                                      child: Text(
                                        item.State_NAME.toString(),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ))
                                .toList(),
                            value: model1,
                            onChanged: (value) {
                              setState(() {
                                isGetState = false;

                                model1 = value!;
                              });
                            },
                            buttonStyleData: ButtonStyleData(
                              height: 50,
                              width: 160,
                              padding:
                                  const EdgeInsets.only(left: 14, right: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                border: Border.all(
                                  color: Colors.black26,
                                ),
                                color: Colors.redAccent,
                              ),
                              elevation: 2,
                            ),
                            iconStyleData: const IconStyleData(
                              icon: Icon(Icons.arrow_drop_down),
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    future:
                        isGetState ? myDatabase.getStateListFromTbl() : null,
                  ),
                ),
                // dropdown close

                // third  country dropdown
                Container(
                  width: double.infinity,
                  child: FutureBuilder<List<Countrymodel>>(
                    builder: (context, snapshot) {
                      print('DEMO::2::${model2.Country_NAME1.toString()}');
                      if (snapshot.data != null && snapshot.hasData) {
                        print('DEMO::3::${model2.Country_NAME1.toString()}');
                        if (isGetCountry) {
                          model2 = snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            items: snapshot.data!
                                .map((item) => DropdownMenuItem<Countrymodel>(
                                      value: item,
                                      child: Text(
                                        item.Country_NAME1.toString(),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ))
                                .toList(),
                            value: model2,
                            onChanged: (value) {
                              setState(() {
                                isGetCountry = false;

                                model2 = value!;
                              });
                            },
                            buttonStyleData: ButtonStyleData(
                              height: 50,
                              width: 160,
                              padding:
                                  const EdgeInsets.only(left: 14, right: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                border: Border.all(
                                  color: Colors.black26,
                                ),
                                color: Colors.greenAccent,
                              ),
                              elevation: 2,
                            ),
                            iconStyleData: const IconStyleData(
                              icon: Icon(Icons.arrow_drop_down),
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    future: isGetCountry
                        ? myDatabase.getCountryLisrFromTbl()
                        : null,
                  ),
                ),
                // thied country dropdown

                //  thied over dropdown

                // realigoin id dropdown
                Container(
                  width: double.infinity,
                  child: FutureBuilder<List<Realigonmodel>>(
                    builder: (context, snapshot) {
                      print('DEMO::2::${model3.Religion_Name1.toString()}');
                      if (snapshot.data != null && snapshot.hasData) {
                        print('DEMO::3::${model3.Religion_Name1.toString()}');
                        if (isGetReligion) {
                          model3 = snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            items: snapshot.data!
                                .map((item) => DropdownMenuItem<Realigonmodel>(
                                      value: item,
                                      child: Text(
                                        item.Religion_Name1.toString(),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ))
                                .toList(),
                            value: model3,
                            onChanged: (value) {
                              setState(() {
                                isGetReligion = false;

                                model3 = value!;
                              });
                            },
                            buttonStyleData: ButtonStyleData(
                              height: 50,
                              width: 160,
                              padding:
                                  const EdgeInsets.only(left: 14, right: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                border: Border.all(
                                  color: Colors.black26,
                                ),
                                color: Colors.orangeAccent,
                              ),
                              elevation: 2,
                            ),
                            iconStyleData: const IconStyleData(
                              icon: Icon(Icons.arrow_drop_down),
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    future: isGetReligion
                        ? myDatabase.getrealigionLisrFromTbl()
                        : null,
                  ),
                ),

                // realigion id over
                // educatio Start
                Container(
                  width: double.infinity,
                  child: FutureBuilder<List<Eduactionmodel>>(
                    builder: (context, snapshot) {
                      print('DEMO::2::${model4.Education_NAME1.toString()}');
                      if (snapshot.data != null && snapshot.hasData) {
                        print('DEMO::3::${model4.Education_NAME1.toString()}');
                        if (isGetEduaction) {
                          model4 = snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            items: snapshot.data!
                                .map((item) => DropdownMenuItem<Eduactionmodel>(
                                      value: item,
                                      child: Text(
                                        item.Education_NAME1.toString(),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ))
                                .toList(),
                            value: model4,
                            onChanged: (value) {
                              setState(() {
                                isGetEduaction = false;

                                model4 = value!;
                              });
                            },
                            buttonStyleData: ButtonStyleData(
                              height: 50,
                              width: 160,
                              padding:
                                  const EdgeInsets.only(left: 14, right: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                border: Border.all(
                                  color: Colors.black26,
                                ),
                                color: Colors.yellow,
                              ),
                              elevation: 2,
                            ),
                            iconStyleData: const IconStyleData(
                              icon: Icon(Icons.arrow_drop_down),
                            ),
                          ),
                        );
                      } else {
                        return Container(
                          color: Colors.blueAccent,
                          height: 10,
                        );
                      }
                    },
                    future: isGetEduaction
                        ? myDatabase.getEduactionLisrFromTbl()
                        : null,
                  ),
                ),
                // eduaction over

                // profession
                Container(
                  width: double.infinity,
                  child: FutureBuilder<List<Professionmodel>>(
                    builder: (context, snapshot) {
                      print('DEMO::2::${model5.Profession_NAME1.toString()}');
                      if (snapshot.data != null && snapshot.hasData) {
                        print('DEMO::3::${model5.Profession_NAME1.toString()}');
                        if (isProfession) {
                          model5 = snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            items: snapshot.data!
                                .map(
                                    (item) => DropdownMenuItem<Professionmodel>(
                                          value: item,
                                          child: Text(
                                            item.Profession_NAME1.toString(),
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ))
                                .toList(),
                            value: model5,
                            onChanged: (value) {
                              setState(() {
                                isProfession = false;

                                model5 = value!;
                              });
                            },
                            buttonStyleData: ButtonStyleData(
                              height: 50,
                              width: 160,
                              padding:
                                  const EdgeInsets.only(left: 14, right: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                border: Border.all(
                                  color: Colors.black26,
                                ),
                                color: Colors.redAccent,
                              ),
                              elevation: 2,
                            ),
                            iconStyleData: const IconStyleData(
                              icon: Icon(Icons.arrow_drop_down),
                            ),
                          ),
                        );
                      } else {
                        return Container(
                          color: Colors.blueAccent,
                          height: 10,
                        );
                      }
                    },
                    future: isProfession
                        ? myDatabase.getprofessionLisrFromTbl()
                        : null,
                  ),
                ),
                // profession over
                // text finld start
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter valid name';
                      } else {
                        return null;
                      }
                    },
                    controller: nameController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your Name',
                      labelText: "Name ",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),
                // TextField(
                //   keyboardType: TextInputType.text,
                //   decoration: InputDecoration(
                //     hintText: 'Enter DOB',
                //     labelText: "Enter your DOB ",
                //     border: OutlineInputBorder(),
                //   ),
                // ),
                // Container(
                //   height: 11,
                // ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your Dob';
                      } else {
                        return null;
                      }
                    },
                    controller: dobController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your dob',
                      labelText: "dob",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your hight';
                      } else {
                        return null;
                      }
                    },
                    controller: heightController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your Hight',
                      labelText: "Height",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your Weight';
                      } else {
                        return null;
                      }
                    },
                    controller: weightController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your Weight',
                      labelText: "Weight",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your State';
                      } else {
                        return null;
                      }
                    },
                    controller: stateController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your State',
                      labelText: "State",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your Country';
                      } else {
                        return null;
                      }
                    },
                    controller: countryController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your Country',
                      labelText: "Country",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your Religion';
                      } else {
                        return null;
                      }
                    },
                    controller: religionController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your Religion',
                      labelText: "Enter Religion",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your Eduction';
                      } else {
                        return null;
                      }
                    },
                    controller: educationController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your Education',
                      labelText: "Education",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your Profession';
                      } else {
                        return null;
                      }
                    },
                    controller: professionController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your Profession',
                      labelText: "Profession ",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value!.trim().length == 0) {
                        return 'Enter your GENDER';
                      } else {
                        return null;
                      }
                    },
                    controller: genderController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Enter your User_GENDER',
                      labelText: "GENDER ",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  height: 11,
                ),
                SizedBox(
                  width: 100,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
                    decoration: BoxDecoration(color: Colors.blue),
                    child: TextButton(
                      onPressed: () {
                        setState(() async {
                          if (_formKey.currentState!.validate()) {
                            if (model.City_ID == -1) {
                              showAlertDialog(context);
                            } else {
                              await myDatabase.upsertIntoUserTable(
                                  cityId: model.City_ID,
                                  userName: nameController.text.toString(),
                                  dob: dobController.text.toString(),
                                  userHeight: heightController.text.toString(),
                                  userWeight: weightController.text.toString(),
                                  stateId: stateController.text.toString(),
                                  CountryId: countryController.text.toString(),
                                  UserReligon:
                                      religionController.text.toString(),
                                  UserEduction:
                                      educationController.text.toString(),
                                  UserProfession:
                                      professionController.text.toString(),
                                  userGender: genderController.text.toString(),
                                  userID: widget.model != null
                                      ? widget.model!.UserID1
                                      : -1);
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => UserListPage()));

                              print('Data to store in DB');
                            }
                          }
                        });
                      },
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context) {
  // set up the button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () {},
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Alert"),
    content: Text("Please select city"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
