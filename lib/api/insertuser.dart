import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Insert_user extends StatefulWidget {
  Insert_user(this.map);

  Map? map;

  @override
  State<Insert_user> createState() => _Insert_userState();
}

class _Insert_userState extends State<Insert_user> {
  var formKey = GlobalKey<FormState>();

  var nameController = TextEditingController();

  var genderController = TextEditingController();

  var cityController = TextEditingController();

  @override
  void initState() {
    super.initState();

    nameController.text = widget.map == null ? '' : widget.map!['Name'];
    genderController.text = widget.map == null ? '' : widget.map!['Gender'];
    cityController.text = widget.map == null ? '' : widget.map!['City'];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(hintText: "Enter Name"),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return "Enter Valid Name";
                  }
                },
                controller: nameController,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: "Enter Gender"),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return "Enter Valid Gender";
                  }
                },
                controller: genderController,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: "Enter City"),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return "Enter Valid City";
                  }
                },
                controller: cityController,
              ),
              TextButton(
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    if (widget.map == null) {
                      insertuser()
                          .then((value) => Navigator.of(context).pop(true));
                    } else {
                      Updateuser(widget.map!['id'])
                          .then((value) => Navigator.of(context).pop(true));
                    }
                  }
                },
                child: Text("Submit"),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> insertuser() async {
    Map map = {};
    print(':::2:::${nameController.text}');
    map["Name"] = nameController.text;
    map["Gender"] = genderController.text;
    map["City"] = cityController.text;

    var responce1 = await http.post(
        Uri.parse("https://6314b7a5fc9dc45cb4f26ed9.mockapi.io/SuperCar"),
        body: map);
    print(responce1.body);
  }

  Future<void> Updateuser(id) async {
    Map map = {};

    map["Name"] = nameController.text;
    map["City"] = cityController.text;
    map["Gender"] = genderController.text;

    var responce1 = await http.put(
        Uri.parse("https://6314b7a5fc9dc45cb4f26ed9.mockapi.io/SuperCar/$id"),
        body: map);
    print(responce1.body);
  }
}
