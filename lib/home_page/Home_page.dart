import 'package:final_app/Adduser.dart';
import 'package:final_app/user_list_page.dart';
import 'package:flutter/material.dart';

class Home_page extends StatefulWidget {
  @override
  State<Home_page> createState() => _Home_pageState();
}

class _Home_pageState extends State<Home_page> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: Stack(
                fit: StackFit.expand,
                children: [
                  // Image.asset('assets/images/',fit: BoxFit.cover,),
                  Image.network(
                    'https://images.unsplash.com/photo-1613935162804-1fe744f47d38?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fGRhdGluZ3xlbnwwfHwwfHw%3D&w=1000&q=80',
                    fit: BoxFit.cover,
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.account_circle),
                  ),
                ),
                Expanded(
                  child: IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => UserListPage()),
                      );
                    },
                    icon: Icon(Icons.list),
                  ),
                ),
                Expanded(
                  child: IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.favorite),
                  ),
                ),
                Expanded(
                  child: IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Adduser(
                                  model: null,
                                )),
                      );
                    },
                    icon: Icon(Icons.add),
                  ),
                ),
                //IconButton(
                //icon: new Icon(Icons.volume_up),
                // alignment: Alignment.center,
                //padding: new EdgeInsets.all(0.0),
                //onPressed: () {},
                //)
              ],
            ),
          ],
        ),
      ),
    );
  }
}
