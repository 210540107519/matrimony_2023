class Countrymodel {
  late int Country_ID1;
  late String Country_NAME1;
  Countrymodel({required this.Country_ID1, required this.Country_NAME1});

  int get Country_ID => Country_ID1;

  set Country_ID(int Country_ID) {
    Country_ID1 = Country_ID;
  }

  String get Country_NAME => Country_NAME1;

  set Country_NAME(String Country_NAME) {
    Country_NAME1 = Country_NAME;
  }
}
