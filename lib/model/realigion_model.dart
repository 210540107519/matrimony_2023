class Realigonmodel {
  late int Religion_ID1;
  late String Religion_Name1;
  Realigonmodel({required this.Religion_ID1, required this.Religion_Name1});

  int get Religion_ID => Religion_ID1;

  set Religion_ID(int Religion_ID) {
    Religion_ID1 = Religion_ID;
  }

  String get Religion_Name => Religion_Name1;

  set Religion_Name(String Religion_Name) {
    Religion_Name1 = Religion_Name;
  }
}
