class Eduactionmodel {
  late int Education_ID1;
  late String Education_NAME1;
  Eduactionmodel({required this.Education_ID1, required this.Education_NAME1});

  int get Education_ID => Education_ID1;

  set Education_ID(int Education_ID) {
    Education_ID1 = Education_ID;
  }

  String get Education_NAME => Education_NAME1;

  set Education_NAME(String Education_NAME) {
    Education_NAME1 = Education_NAME;
  }
}
