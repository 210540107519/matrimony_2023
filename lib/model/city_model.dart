class CityModel {
  late int CityID1;
  late String CityName1;
  CityModel({required this.CityID1, required this.CityName1});
  int get City_ID => CityID1;

  set City_ID(int City_ID) {
    CityID1 = City_ID;
  }

  String get City_NAME => CityName1;

  set City_NAME(String City_NAME) {
    CityName1 = City_NAME;
  }
}
