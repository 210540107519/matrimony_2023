class Professionmodel {
  late int Profession_ID1;
  late String Profession_NAME1;
  Professionmodel(
      {required this.Profession_ID1, required this.Profession_NAME1});

  int get Profession_ID => Profession_ID1;

  set Profession_ID(int Profession_ID) {
    Profession_ID1 = Profession_ID;
  }

  String get Profession_NAME => Profession_NAME1;

  set Profession_NAME(String Profession_NAME) {
    Profession_NAME1 = Profession_NAME;
  }
}
