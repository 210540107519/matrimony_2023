class UserModel {
  late int UserID1;
  late String User_NAME1;
  late String User_DOB1;
  late String User_GENDER1;
  late String User_HEIGHT1;
  late String User_WEIGHT1;
  late int Country_ID1;
  late int State_ID1;
  late int City_ID1;
  late String Religion_ID1;
  late String Education_ID1;
  late String Profession_ID1;
  late bool _isFavouriteUser;

  bool get isFavouriteUser => _isFavouriteUser;

  set isFavouriteUser(bool isFavouriteUser) {
    _isFavouriteUser = isFavouriteUser;
  }

  UserModel(
      {required this.Country_ID1,
      required this.City_ID1,
      required this.UserID1,
      required this.Education_ID1,
      required this.Profession_ID1,
      required this.Religion_ID1,
      required this.State_ID1,
      required this.User_DOB1,
      required this.User_GENDER1,
      required this.User_HEIGHT1,
      required this.User_NAME1,
      required this.User_WEIGHT1});

  int get User_ID => UserID1;

  set User_ID(int User_ID) {
    UserID1 = User_ID;
  }

  String get User_NAME => User_NAME1;

  set User_NAME(String User_NAME) {
    User_NAME1 = User_NAME;
  }

  String get User_DOB => User_DOB1;

  set User_DOB(String User_DOB) {
    User_DOB1 = User_DOB;
  }

  String get User_GENDER => User_GENDER;

  set User_GENDER(String User_GENDER) {
    User_GENDER1 = User_GENDER1;
  }

  String get User_HEIGHT => User_HEIGHT1;

  set User_HEIGHT(String User_HEIGHT) {
    User_HEIGHT1 = User_HEIGHT;
  }

  String get User_WEIGHT => User_WEIGHT1;

  set User_WEIGHT(String User_WEIGHT) {
    User_WEIGHT1 = User_WEIGHT;
  }

  int get Country_ID => Country_ID1;

  set Country_ID(int Country_ID) {
    Country_ID1 = Country_ID;
  }

  int get State_ID => State_ID1;

  set State_ID(int State_ID) {
    State_ID1 = State_ID;
  }

  int get City_ID => City_ID1;

  set City_ID(int City_ID) {
    City_ID1 = City_ID;
  }

  String get Religion_ID => Religion_ID1;

  set Religion_ID(String Religion_ID) {
    Religion_ID1 = Religion_ID;
  }

  String get Education_ID => Education_ID1;

  set Education_ID(String Education_ID) {
    Education_ID1 = Education_ID;
  }

  String get Profession_ID => Profession_ID1;

  set Profession_ID(String Profession_ID) {
    Profession_ID1 = Profession_ID;
  }
}
