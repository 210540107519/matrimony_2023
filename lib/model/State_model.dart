class statemodel {
  late int State_ID1;
  late String State_NAME1;
  statemodel({required this.State_ID1, required this.State_NAME1});
  int get State_ID => State_ID1;

  set State_ID(int State_ID) {
    State_ID1 = State_ID;
  }

  String get State_NAME => State_NAME1;

  set State_NAME(String State_NAME) {
    State_NAME1 = State_NAME;
  }
}
