import 'package:final_app/Adduser.dart';
import 'package:final_app/api/api_crud.dart';
import 'package:final_app/user_list_page.dart';
import 'package:flutter/material.dart';

import 'home_page/Home_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home_page(),
    );
  }
}
