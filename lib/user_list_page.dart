import 'package:final_app/Adduser.dart';
import 'package:final_app/database/database.dart';
import 'package:final_app/favourite.dart';
import 'package:final_app/model/city_model.dart';
import 'package:final_app/model/user_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  MyDatabase db = MyDatabase();
  List<UserModel> localList = [];
  List<UserModel> searchData = [];
  List<UserModel> favourite = [];
  bool isGetData = true;
  TextEditingController controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    controller.addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: const [
            Icon(
              Icons.person,
              color: Colors.white,
            ),
            SizedBox(
              width: 5,
            ),
            Text('User List')
          ],
        ),
      ),
      body: FutureBuilder<bool>(
          builder: (context, snapshot1) {
            if (snapshot1.hasData) {
              return FutureBuilder<List<UserModel>>(
                builder: (context, snapshot) {
                  if (snapshot != null && snapshot.hasData) {
                    if (isGetData) {
                      localList.addAll(snapshot.data!);
                      searchData.addAll(localList);
                    }

                    isGetData = false;
                    return Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        TextField(
                          controller: controller,
                          onSubmitted: (data) {
                            searchData.clear();
                            controller.text = data;
                            print("data-=$data");
                            for (int i = 0; i < localList.length; i++) {
                              if (localList[i]
                                  .User_NAME1
                                  .toLowerCase()
                                  .contains(data.toLowerCase())) {
                                searchData.add(localList[i]);
                              }
                            }
                            setState(() {});
                          },
                          decoration:
                              const InputDecoration(hintText: "Search User"),
                        ),
                        controller.text.isNotEmpty
                            ? Expanded(
                                child: ListView.builder(
                                  padding: const EdgeInsets.all(5),
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                          builder: (context) => Adduser(
                                            model: searchData![index],
                                          ),
                                        ));
                                      },
                                      child: Card(
                                        color: Colors.grey,
                                        elevation: 3.0,
                                        borderOnForeground: true,
                                        child: Container(
                                          padding: const EdgeInsets.all(5),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      (searchData![index]
                                                          .User_NAME1
                                                          .toString()),
                                                    ),
                                                    Text(
                                                      (searchData![index]
                                                          .User_DOB1
                                                          .toString()),
                                                      style: const TextStyle(
                                                          color: Colors
                                                              .orangeAccent,
                                                          fontSize: 12),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              IconButton(
                                                  onPressed: () {
                                                    showAlertDialog(
                                                        context, index);
                                                  },
                                                  icon:
                                                      const Icon(Icons.delete)),
                                              const Icon(Icons
                                                  .keyboard_arrow_right_sharp),
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    searchData[index]
                                                            .isFavouriteUser =
                                                        !searchData[index]
                                                            .isFavouriteUser;
                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    Favourite(
                                                                      favourite:
                                                                          favourite,
                                                                    )));

                                                    favourite
                                                        .add(searchData[index]);
                                                  });
                                                },
                                                child: Icon(
                                                  searchData[index]
                                                          .isFavouriteUser
                                                      ? Icons.favorite
                                                      : Icons.favorite_border,
                                                  color: Colors.red,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount: searchData!.length,
                                ),
                              )
                            : Expanded(
                                child: ListView.builder(
                                  padding: const EdgeInsets.all(5),
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                          builder: (context) => Adduser(
                                            model: localList![index],
                                          ),
                                        ));
                                      },
                                      child: Card(
                                        color: Colors.grey,
                                        elevation: 3.0,
                                        borderOnForeground: true,
                                        child: Container(
                                          padding: const EdgeInsets.all(5),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      (localList![index]
                                                          .User_NAME1
                                                          .toString()),
                                                    ),
                                                    Text(
                                                      (localList![index]
                                                          .User_DOB1
                                                          .toString()),
                                                      style: const TextStyle(
                                                          color: Colors
                                                              .orangeAccent,
                                                          fontSize: 12),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              IconButton(
                                                  onPressed: () {
                                                    showAlertDialog(
                                                        context, index);
                                                  },
                                                  icon:
                                                      const Icon(Icons.delete)),
                                              const Icon(Icons
                                                  .keyboard_arrow_right_sharp),
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    Favourite(
                                                                      favourite:
                                                                          favourite,
                                                                    )));
                                                    localList[index]
                                                            .isFavouriteUser =
                                                        !localList[index]
                                                            .isFavouriteUser;
                                                    favourite
                                                        .add(localList[index]);
                                                  });
                                                },
                                                child: Icon(
                                                  localList[index]
                                                          .isFavouriteUser
                                                      ? Icons.favorite
                                                      : Icons.favorite_border,
                                                  color: Colors.red,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount: localList!.length,
                                ),
                              ),
                      ],
                    );
                  } else {
                    return const Center(
                      child: Text('No Found User'),
                    );
                  }
                },
                future: isGetData ? db.getUserListFromTbl() : null,
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
          future: db.copyPasteAssetFileToRoot()),
    );
  }

  showAlertDialog(BuildContext context, index) {
    Widget yesButton = TextButton(
      child: const Text("Yes"),
      onPressed: () async {
        int deleteUserId =
            await db.deleteUserFromUserTable(localList[index].UserID1);
        if (deleteUserId > 0) {
          localList.removeAt(index);
        }
        Navigator.pop(context);
        setState(() {});
      },
    );
    Widget noButton = TextButton(
      child: const Text("No"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: const Text("Alert"),
      content: const Text("Are you sure want to delete?"),
      actions: [
        yesButton,
        noButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
