import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Adduser.dart';
import 'model/user_model.dart';

class Favourite extends StatelessWidget {
  final List<UserModel> favourite;
  const Favourite({Key? key, required this.favourite}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FavroutList'),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.all(5),
        itemBuilder: (context, index) {
          return Card(
            color: Colors.grey,
            elevation: 3.0,
            borderOnForeground: true,
            child: Container(
              padding: const EdgeInsets.all(5),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          (favourite![index].User_NAME1.toString()),
                        ),
                        Text(
                          (favourite![index].User_DOB1.toString()),
                          style: const TextStyle(
                              color: Colors.orangeAccent, fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  const Icon(Icons.keyboard_arrow_right_sharp),
                  InkWell(
                    onTap: () {},
                    child: Icon(
                      favourite[index].isFavouriteUser
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
        itemCount: favourite!.length,
      ),
    );
    ;
  }
}
