import 'dart:io';
import 'dart:typed_data';
import 'package:final_app/model/Eduaction_model.dart';
import 'package:final_app/model/State_model.dart';
import 'package:final_app/model/city_model.dart';
import 'package:final_app/model/country_model.dart';
import 'package:final_app/model/profession_model.dart';
import 'package:final_app/model/realigion_model.dart';
import 'package:final_app/model/user_model.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class MyDatabase {
  Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'Matrimony_app.db');
    return await openDatabase(
      databasePath,
      version: 2,
    );
  }

  Future<bool> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "Matrimony_app.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data =
          await rootBundle.load(join('assets/database', 'Matrimony_app.db'));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await new File(path).writeAsBytes(bytes);
      return true;
    }
    return false;
  }

  // insert qurry
  Future<void> upsertIntoUserTable(
      {cityId,
      userName,
      userID,
      CountryId,
      stateId,
      userGender,
      userHeight,
      userWeight,
      dob,
      UserEduction,
      UserProfession,
      UserReligon}) async {
    Database db = await initDatabase();
    Map<String, Object?> map = Map();
    map['User_NAME'] = userName;
    map['City_ID'] = cityId;
    map['User_DOB'] = dob;
    map['User_GENDER'] = userGender;
    map['User_HEIGHT'] = userHeight;
    map['User_WEIGHT'] = userWeight;
    map['Country_ID'] = CountryId;
    map['State_ID'] = stateId;
    map['Religion_ID'] = UserReligon;
    map['Education_ID'] = UserEduction;
    map['Profession_ID'] = UserProfession;
    if (userID != -1) {
      map['User_ID'] = userID;
      await db
          .update('TBL_USER', map, where: 'User_ID = ?', whereArgs: [userID]);
    } else {
      await db.insert('TBL_USER', map);
    }
  }

  //insert ovewr

  Future<List<CityModel>> getCityListFromTbl() async {
    List<CityModel> cityList = [];
    print("USER LIST :1::");
    Database db = await initDatabase();
    print("USER LIST :2::");
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_CITY');
    print("USER LIST :3::");
    CityModel model = CityModel(CityID1: -1, CityName1: 'select city');
    //print("USER LIST :4::");
    cityList.add(model);
    //print("USER LIST :5::${data.length}");
    for (int i = 0; i < data.length; i++) {
      //print("USER LIST :6::${data.length}");
      model = CityModel(
          CityID1: int.parse(data[i]['City_ID'].toString()),
          CityName1: data[i]['City_NAME'].toString());
      // print("USER LIST :7::${data.length}");
      cityList.add(model);
    }
    // print("USER LIST :::${data.length}");
    return cityList;
  }

  Future<List<statemodel>> getStateListFromTbl() async {
    List<statemodel> statelist = [];
    print("USER LIST :1::");
    Database db = await initDatabase();
    print("USER LIST :2::");
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_STATE');
    print("USER LIST :3::");
    statemodel model1 = statemodel(State_ID1: -1, State_NAME1: 'Select State');
    //print("USER LIST :4::");
    statelist.add(model1);
    //print("USER LIST :5::${data.length}");
    for (int i = 0; i < data.length; i++) {
      //print("USER LIST :6::${data.length}");
      model1 = statemodel(
          State_ID1: data[i]['State_ID'] as int,
          State_NAME1: data[i]['State_NAME'].toString());
      // print("USER LIST :7::${data.length}");
      statelist.add(model1);
    }
    // print("USER LIST :::${data.length}");
    return statelist;
  }

  Future<List<Countrymodel>> getCountryLisrFromTbl() async {
    List<Countrymodel> CountryList = [];
    print("USER LIST :1::");
    Database db = await initDatabase();
    print("USER LIST :2::");
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_COUNTRY');
    print("USER LIST :3::");
    Countrymodel model2 =
        Countrymodel(Country_ID1: -1, Country_NAME1: 'Select Country');
    //print("USER LIST :4::");
    CountryList.add(model2);
    //print("USER LIST :5::${data.length}");
    for (int i = 0; i < data.length; i++) {
      //print("USER LIST :6::${data.length}");
      model2 = Countrymodel(
          Country_ID1: data[i]['Country_ID'] as int,
          Country_NAME1: data[i]['Country_NAME'].toString());
      // print("USER LIST :7::${data.length}");
      CountryList.add(model2);
    }
    // print("USER LIST :::${data.length}");
    return CountryList;
  }

  Future<List<Realigonmodel>> getrealigionLisrFromTbl() async {
    List<Realigonmodel> RealigionList = [];
    print("USER LIST :1::");
    Database db = await initDatabase();
    print("USER LIST :2::");
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_RELIGION ');
    print("USER LIST :3::");
    Realigonmodel model3 =
        Realigonmodel(Religion_ID1: -1, Religion_Name1: 'Select religion');
    //print("USER LIST :4::");
    RealigionList.add(model3);
    //print("USER LIST :5::${data.length}");
    for (int i = 0; i < data.length; i++) {
      //print("USER LIST :6::${data.length}");
      model3 = Realigonmodel(
          Religion_ID1: data[i]['Religion_ID'] as int,
          Religion_Name1: data[i]['Religion_Name'].toString());
      // print("USER LIST :7::${data.length}");
      RealigionList.add(model3);
    }
    // print("USER LIST :::${data.length}");
    return RealigionList;
  }

  // eduction
  Future<List<Eduactionmodel>> getEduactionLisrFromTbl() async {
    List<Eduactionmodel> EducationList = [];
    print("USER LIST :1::");
    Database db = await initDatabase();
    print("USER LIST :2::");
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_EDUCATION ');
    print("USER LIST :3::");
    Eduactionmodel model4 =
        Eduactionmodel(Education_ID1: -1, Education_NAME1: 'select education');
    //print("USER LIST :4::");
    EducationList.add(model4);
    //print("USER LIST :5::${data.length}");
    for (int i = 0; i < data.length; i++) {
      //print("USER LIST :6::${data.length}");
      model4 = Eduactionmodel(
          Education_ID1: data[i]['Education_ID'] as int,
          Education_NAME1: data[i]['Education_NAME'].toString());
      // print("USER LIST :7::${data.length}");
      EducationList.add(model4);
    }
    // print("USER LIST :::${data.length}");
    return EducationList;
  }
  // education over

  // profession model
  Future<List<Professionmodel>> getprofessionLisrFromTbl() async {
    List<Professionmodel> professionList = [];
    print("USER LIST :1::");
    Database db = await initDatabase();
    print("USER LIST :2::");
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_PROFESSION ');
    print("USER LIST :3::");
    Professionmodel model5 = Professionmodel(
        Profession_ID1: -1, Profession_NAME1: 'select education');
    //print("USER LIST :4::");
    professionList.add(model5);
    //print("USER LIST :5::${data.length}");
    for (int i = 0; i < data.length; i++) {
      //print("USER LIST :6::${data.length}");
      model5 = Professionmodel(
          Profession_ID1: data[i]['Profession_ID'] as int,
          Profession_NAME1: data[i]['Profession_NAME'].toString());
      // print("USER LIST :7::${data.length}");
      professionList.add(model5);
    }
    // print("USER LIST :::${data.length}");
    return professionList;
  }
  // profession model over

  Future<List<UserModel>> getUserListFromTbl() async {
    List<UserModel> userList = [];
    print("USER LIST :1::");
    Database db = await initDatabase();
    print("USER LIST :2::");
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_USER');
    print("USER LIST :3::");

    print("USER LIST :4::${data.length}");
    for (int i = 0; i < data.length; i++) {
      //print("USER LIST :6::${data.length}");
      print("User:::::::$i");
      print("User:::::::1:::${data[i]['Country_ID'] as int}");
      print("User:::::::2:::${data[i]['City_ID'] as int}");
      print("User:::::::3:::${data[i]['Education_ID'].toString()}");
      print("User:::::::4:::${data[i]['Profession_ID'].toString()}");
      print("User:::::::5:::${data[i]['Religion_ID'].toString()}");
      print("User:::::::6:::${data[i]['State_ID'] as int}");
      print("User:::::::7:::${data[i]['User_DOB'].toString()}");
      print("User:::::::8:::${data[i]['User_GENDER'].toString()}");
      // print("User:::::::9:::${data[i]['User_GENDER'] as int}");
      print("User:::::::10:::${data[i]['User_WEIGHT'].toString()}");
      UserModel model = UserModel(
          UserID1: data[i]['User_ID'] as int,
          Country_ID1: data[i]['Country_ID'] as int,
          City_ID1: data[i]['City_ID'] as int,
          Education_ID1: data[i]['Education_ID'].toString(),
          Profession_ID1: data[i]['Profession_ID'].toString(),
          Religion_ID1: data[i]['Religion_ID'].toString(),
          State_ID1: data[i]['State_ID'] as int,
          User_DOB1: data[i]['User_DOB'].toString(),
          User_GENDER1: data[i]['User_GENDER'].toString(),
          User_HEIGHT1: data[i]['User_HEIGHT'].toString(),
          User_NAME1: data[i]['User_NAME'].toString(),
          User_WEIGHT1: data[i]['User_WEIGHT'].toString());

      // model.User_NAME = data[i]['User_NAME'].toString();
      // //model.User_DOB = data[i]['User_DOB'].toString();
      // model.User_HEIGHT = data[i]['User_DOB'].toString();
      // model.User_WEIGHT = data[i]['User_WEIGHT'].toString();
      // model.City_ID = data[i]['CityID'] as int;
      // model.State_ID = data[i]['State_ID'] as int;
      // model.Country_ID = data[i]['Country_ID'] as int;
      // model.Religion_ID = data[i]['Religion_ID'].toString();
      // model.Education_ID = data[i]['Education_ID'].toString();
      // model.Profession_ID = data[i]['Profession_ID'].toString();
      model.isFavouriteUser = false;
      print("USER LIST :5::");
      userList.add(model);
    }
    // print("USER LIST :::${data.length}");
    return userList;
  }

  Future<int> deleteUserFromUserTable(User_ID) async {
    Database db = await initDatabase();
    int deletedid = await db.delete(
      'TBL_USER',
      where: 'User_ID = ?',
      whereArgs: [User_ID],
    );
    return deletedid;
  }
}
